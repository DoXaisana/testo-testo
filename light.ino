int led[] = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
int numLeds = sizeof(led) / sizeof(led[0]);
//Mr. Xaisana Lattaphon N0.30
void lighting() {
  for (int i = 0; i < numLeds; ++i) {
    digitalWrite(led[i], HIGH);
    delay(250);
    digitalWrite(led[i], LOW);
    delay(250);
  }
  for (int i = numLeds - 1; i >= 0; --i) {
    digitalWrite(led[i], HIGH);
    delay(250);
    digitalWrite(led[i], LOW);
    delay(250);
  }
}


void lightIn() {
  int start = 0;
  int end = numLeds - 1;

  while (start < end) {
    digitalWrite(led[start], HIGH);  
    digitalWrite(led[end], HIGH);    
    delay(250);
    digitalWrite(led[start], LOW);   
    digitalWrite(led[end], LOW);    
    delay(250);

    start++;
    end--;
  }
}

void lightOut() {
  int start = numLeds / 2 - 1; 
  int end = numLeds / 2;       
  
  while (start >= 0 && end < numLeds) {
    digitalWrite(led[start], HIGH);
    digitalWrite(led[end], HIGH);    
    delay(250);
    digitalWrite(led[start], LOW);   
    digitalWrite(led[end], LOW); 
    delay(250);

    start--;
    end++;
  }
}




void loop() {
  lighting();
  lightIn();
  lightOut();
}